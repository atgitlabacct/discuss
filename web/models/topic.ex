defmodule Discuss.Topic do
  @moduledoc """
  Topic model
  """

  use Discuss.Web, :model

  alias Discuss.Topic

  schema "topics" do
    field :title, :string
  end

  @doc """
  Changes a Map of data into a Changset for Topic
  """
  def changeset(%Topic{} = struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
  end

end
