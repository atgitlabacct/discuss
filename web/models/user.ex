defmodule Discuss.User do
  @moduledoc """
  Represents a user schema
  """
  use Discuss.Web, :model

  schema "users" do
    field :email, :string
    field :provider, :string
    field :token, :string

    timestamps
  end

  @required [:email, :provider, :token]

  def changeset(user, params \\ %{}) do
    user
    |> cast(params, @required)
    |> validate_required(@required)
  end
end
