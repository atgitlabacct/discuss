defmodule Discuss.IsAuthenticated do
  import Discuss.Router.Helpers, only: [topic_path: 2]
  import Phoenix.Controller, only: [redirect: 2, put_flash: 3]
  import Plug.Conn, only: [get_session: 2, halt: 1]

  def init(_conn) do
  end

  def call(conn, _params) do
    if get_session(conn, :user_id)  do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in.")
      |> redirect(to: topic_path(conn, :index))
      |> halt
    end
  end
end
