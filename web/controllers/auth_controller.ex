defmodule Discuss.AuthController do
  use Discuss.Web, :controller
  plug Ueberauth

  alias Discuss.{User, Repo}
  alias Plug.Conn

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    user_params = %{token: auth.credentials.token, email: auth.info.email,
     provider: to_string(auth.provider)}

    user_cs = User.changeset(%User{}, user_params)
    signin(conn, user_cs)
  end

  def signout(conn, _parms) do
    conn
    |> Conn.configure_session(drop: true)
    |> redirect(to: topic_path(conn, :index))
  end

  defp signin(conn, user_cs) do
    case insert_or_update_user(user_cs) do
      {:error, _reason} ->
        conn
        |> put_flash(:error, 'Could not login')
        |> redirect(to: root_path(conn, :index))
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> put_flash(:info, "Login successfull")
        |> redirect(to: root_path(conn, :index))
    end
  end

  defp insert_or_update_user(user_cs) do
    case Repo.get_by(User, email: user_cs.changes.email) do
      nil ->
        Repo.insert(user_cs)
      user ->
        {:ok, user}
    end
  end
end
