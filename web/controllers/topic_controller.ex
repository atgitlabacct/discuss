defmodule Discuss.TopicController do
  use Discuss.Web, :controller
  plug Discuss.IsAuthenticated when action in [:new, :create, :edit, :update, :delete]

  alias Discuss.{Topic, Repo}

  def index(conn, _params) do
    render conn, topics: Repo.all(Topic), changeset: Topic.changeset(%Topic{})
  end

  def new(conn, _params) do
    changeset = Topic.changeset %Topic{}, %{}
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"topic" => topic}) do
    cs = Topic.changeset %Topic{}, topic
    case Repo.insert(cs) do
      {:error, cs} ->
        render conn, "new.html", changeset: cs
      {:ok, topic} ->
        redirect conn, to: topic_path(conn, :show, topic)
    end
  end

  def edit(conn, %{"id" => id}) do
    case Repo.get(Topic, id) do
      nil ->
        conn
        |> put_flash(:error, "Topic with id of #{id} could not be found")
        |> redirect(to: topic_path(conn, :index))
      topic ->
        render conn, "edit.html", changeset: Topic.changeset(topic), topic: topic
    end
  end

  def update(conn, %{"id" => id, "topic" => topic_params}) do
    topic = Repo.get(Topic, id)
    changeset = Topic.changeset(topic, topic_params)

    case Repo.update(changeset) do
      {:ok, _topic} ->
        conn
        |> put_flash(:info, "Topic updated!")
        |> redirect(to: topic_path(conn, :index))
      {:error, changeset} ->
        conn
        |> put_flash(:error, "Unable to update Topic")
        |> render("edit.html", changeset: changeset, topic: topic)
    end
  end

  def show(conn, %{"id" => id}) do
    case Repo.get(Topic, id) do
      nil ->
        redirect conn, to: topic_path(conn, :new)
      topic ->
        render conn, "show.html", topic: topic
    end
  end

  def delete(conn, %{"id" => id}) do
    topic = Repo.get(Topic, id)
    case Repo.delete(topic) do
      {:error, _cs} ->
        put_flash(conn, :flash, "Unable to remove topic with id #{id}")
        redirect conn, to: topic_path(conn, :index)
      {:ok, topic} ->
        put_flash(conn, :flash, "Topic #{topic.title} has been deleted")
        redirect conn, to: topic_path(conn, :index)
    end
  end
end
