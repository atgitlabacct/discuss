
defmodule Address do
  use Ecto.Schema

  embedded_schema do
    field :street1, :string
    field :street2, :string
    field :street3, :string
    field :city, :string
    field :state, :string
    field :postal_code, :string
  end

  @allowed [:street1, :street2, :street3, :city, :state, :postal_code]

  def changeset(schema, params, allowed \\ @allowed) do
    schema
    |> Ecto.Changeset.cast(params, allowed)
    |> Ecto.Changeset.validate_required([:street1, :city, :state, :postal_code])
  end
end

defmodule Person do
  use Ecto.Schema

  embedded_schema do
    field :name, :string
    field :age, :integer
    embeds_one :primary_address, Address
    embeds_one :secondary_address, Address
  end
end
